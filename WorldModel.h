#pragma once
#include "Action.h"

class WorldModel
{
private:
	// a list of all actions
	std::vector<Action> actions;
	
public:
	// list of all current goals and their current values in this worldmodel
	std::vector<Goal> goals;

	// pass action vector by reference, since the actions are never changed, the goals are though
	explicit WorldModel(std::vector<Action>& actions, std::vector<Goal> goals);
	WorldModel(const WorldModel& obj);
	~WorldModel();

	// heuristic estimate of the cost to the "goalstate" which may comprise of multiple goal-values
	float heuristic(const std::vector<Goal>& goalsToAchieve) const;
	bool goalsAchieved(const std::vector<Goal>& goalsToAchieve) const;
	void output() const;

	std::vector<Action>& getActions();
	void addAction(Action action);
	void applyAction(Action& action);
};

