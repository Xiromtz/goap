#pragma once
#include <typeindex>
#include <string>

class Goal
{
private:
	
public:
	// the current value of the goal
	// if its a "normal" goal inside of a worldmodel, this has a initialvalue and gains value over time
	// special goals start with a value of 1 and are completed with a value of 0 and are always done in 1 step
	float value;

	// name for debugging and comparison purposes
	std::string name;

	// Every time an action takes place, the timeChange value is multiplied by the time an action takes (removed due to extremely long execution time)
	float timeChange;

	explicit Goal(const std::string& name, float value, float timeChange = 0);
	Goal(){};
	Goal(const Goal& obj);
	~Goal();

	bool operator==(const Goal& other) const
	{
		return (other.value == value && other.name == name);
	}
};

namespace std
{
	template<>
	struct hash<Goal>
	{
		std::size_t operator()(const Goal& key) const
		{
			return ((hash<string>()(key.name)
				^ (hash<float>()(key.value) << 1)) >> 1);
		}
	};
}