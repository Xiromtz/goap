#pragma once
#include "WorldModel.h"

class Planer
{
public:
	Planer();
	~Planer();
	std::vector<Action> planAction(WorldModel& model, const std::vector<Goal>& goalsToFulfill);
	float search(std::vector<Action>& path, float currentCost, float bound, const std::vector<Goal>& goalsToFulfill, WorldModel& model);
};

