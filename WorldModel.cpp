#include "stdafx.h"
#include "WorldModel.h"
#include <iostream>


WorldModel::WorldModel(std::vector<Action>& actions, std::vector<Goal> goals) : goals(goals), actions(actions)
{
	
}

WorldModel::WorldModel(const WorldModel& obj)
{
	actions = obj.actions;
	goals = obj.goals;
}


WorldModel::~WorldModel()
{
}

void WorldModel::addAction(Action action)
{
	actions.push_back(action);
}

std::vector<Action>& WorldModel::getActions()
{
	return actions;
}

void WorldModel::applyAction(Action& action)
{
	for (auto it = goals.begin(); it != goals.end(); ++it)
	{
		// Had to comment this out, as computation time turns out to be way too long with time turned on
		//it->value += action.duration * it->timeChange;
		it->value += action.getGoalChange(it->name);
	}
}

bool WorldModel::goalsAchieved(const std::vector<Goal>& goalsToAchieve) const
{
	for (auto it1 = goalsToAchieve.begin(); it1 != goalsToAchieve.end(); ++it1)
	{
		for (auto it2 = goals.begin(); it2 != goals.end(); ++it2)
		{
			if (it2->name == it1->name)
			{
				if (it2->value > it1->value)
				{
					return false;
				}
			}
		}
	}
	return true;
}


float WorldModel::heuristic(const std::vector<Goal>& goalsToAchieve) const
{
	float val = 0;
	for (Goal goalToAchieve : goalsToAchieve)
	{
		for (Goal goal : goals)
		{
			if (goal.name == goalToAchieve.name)
			{
				if (goal.value > goalToAchieve.value)
				{
					float diff = goal.value - goalToAchieve.value;
					if (goal.name == "zombies_dead")
					{
						Goal hasShotGun;
						Goal cupboardOpen;
						for (Goal tempGoal : goals)
						{
							if (tempGoal.name == "has_weapon")
							{
								hasShotGun = tempGoal;
							}
							else if (tempGoal.name == "cupboard_open")
							{
								cupboardOpen = tempGoal;
							}
						}

						Action shoot;
						Action getWeapon;
						Action openCupboard;

						for (Action action : actions)
						{
							if (action.name == "kill zombie")
							{
								shoot = action;
							}
							else if (action.name == "get weapon")
							{
								getWeapon = action;
							}
							else if (action.name == "open cupboard")
							{
								openCupboard = action;
							}
						}

						float multiplier = (hasShotGun.value * getWeapon.duration) + (cupboardOpen.value * openCupboard.duration);
						val += multiplier + (diff/abs(shoot.getGoalChange(goal.name))) * shoot.duration;
					}
					else if (goal.name == "hunger")
					{
						Goal hasPizza;
						for (Goal tempGoal : goals)
						{
							if (tempGoal.name == "has_baked_pizza")
							{
								hasPizza = tempGoal;
							}
						}
						Action bakePizza;
						Action eatPizza;
						for (Action action : actions)
						{
							if (action.name == "bake pizza")
							{
								bakePizza = action;
							}
							else if (action.name == "eat pizza")
							{
								eatPizza = action;
							}
						}

						val += bakePizza.duration * hasPizza.value + eatPizza.duration * (diff/abs(eatPizza.getGoalChange(goal.name)));
					}
					else if (goal.name == "thirst")
					{
						Goal hasDrink;
						for (Goal tempGoal : goals)
						{
							if (tempGoal.name == "has_drink")
							{
								hasDrink = tempGoal;
							}
						}

						Action drink;
						Action getDrink;
						for (Action action : actions)
						{
							if (action.name == "drink water")
							{
								drink = action;
							}
							else if (action.name == "fill glass")
							{
								getDrink = action;
							}
						}

						val += hasDrink.value * getDrink.duration + (diff/abs(drink.getGoalChange(goal.name))) * drink.duration;
					}
					else if (goal.name == "toilet")
					{
						Action goToToilet;
						for (Action action : actions)
						{
							if (action.name == "go to toilet")
							{
								goToToilet = action;
							}
						}

						val += (diff/abs(goToToilet.getGoalChange(goal.name))) * goToToilet.duration;
					}
					else if (goal.name == "sleep")
					{
						Action goToSleep;
						for (Action action : actions)
						{
							if (action.name == "go to sleep")
							{
								goToSleep = action;
							}
						}
						
						val += (diff/abs(goToSleep.getGoalChange(goal.name))) * goToSleep.duration;
					}
				}
			}
		}
	}
	return val;
}

void WorldModel::output() const
{
	for (Goal goal : goals)
	{
		std::cout << "Goal " << goal.name << " is currently at value: " << goal.value << std::endl;
	}
}
