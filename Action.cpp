#include "stdafx.h"
#include "Action.h"


Action::Action(float duration, const std::string name) : duration(duration), name(name)
{
}

Action::~Action()
{
}

void Action::setGoalChange(const std::string& goalName, float change)
{
	goalChanges[goalName] = change;
}

float Action::getGoalChange(const std::string& goalName)
{
	for (auto it = goalChanges.begin(); it != goalChanges.end(); ++it)
	{
		if (it->first == goalName)
		{
			return it->second;
		}
	}
	return 0;
}

bool Action::requirementsFulfilled(const std::vector<Goal>& goals)
{
	for (auto it = requirements.begin(); it != requirements.end(); ++it)
	{
		bool requirementMet = false;
		for (auto it1 = goals.begin(); it1 != goals.end(); ++it1)
		{
			if (it->first == it1->name)
			{
				if (it1->value <= it->second)
					requirementMet = true;
			}
		}

		if (!requirementMet)
			return false;
	}

	for (auto it = goalChanges.begin(); it != goalChanges.end(); ++it)
	{
		if (it->second < 0)
		{
			for (auto it1 = goals.begin(); it1 != goals.end(); ++it1)
			{
				if (it->first == it1->name)
				{
					if (it1->value <= 0)
					{
						return false;
					}
				}
			}
		}
	}

	return true;
}

void Action::addRequirement(const std::string& goalName, float value)
{
	requirements[goalName] = value;
}
