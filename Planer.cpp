#include "stdafx.h"
#include "Planer.h"
#include <iostream>


Planer::Planer()
{
}


Planer::~Planer()
{
}

std::vector<Action> Planer::planAction(WorldModel& model, const std::vector<Goal>& goalsToFulfill)
{
	
	float bound = model.heuristic(goalsToFulfill);

	std::vector<Action> path;
	while (true)
	{
		float t = search(path, 0, bound, goalsToFulfill, model);
		if (t == -1)
		{
			return path;
		}
		if (t == -2)
			return std::vector<Action>();

		bound = t;
	}
}

float Planer::search(std::vector<Action>& path, float currentCost, float bound, const std::vector<Goal>& goalsToFulfill, WorldModel& model)
{
	if (model.goalsAchieved(goalsToFulfill))
	{
		return -1;
	}


	float cost = currentCost + model.heuristic(goalsToFulfill);
	if (cost > bound)
		return cost;

	float min = -2;
	std::vector<Action> actions = model.getActions();
	for (Action& nextAction : actions)
	{
		if (nextAction.requirementsFulfilled(model.goals))
		{
			WorldModel next(model);
			next.applyAction(nextAction);
			
			path.push_back(nextAction);
			float searchResult = search(path, currentCost + nextAction.duration, bound, goalsToFulfill, next);
			if (searchResult == -1)
				return -1;
			if (searchResult < min || min == -2)
				min = searchResult;
			path.pop_back();
		}
	}
	return min;
}