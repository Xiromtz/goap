#pragma once

#include "Goal.h"
#include <map>
#include <vector>

class Action
{
private:
	// An action may have requirements, which comprise of a goal name as a string and a float value to specify the exact value that is needed
	std::map<std::string, float> requirements;
	// Again, a map with a string for the goal name and a float for the value. This time, the value specifies how much the specific goal should change once this action is applied
	std::map<std::string, float> goalChanges;

public:

	// name for comparison and debugging
	std::string name;
	// specifies the time this action takes; this is equivalent to the cost and is used as such during pathfinding
	float duration;
	explicit Action(float duration, const std::string name);
	Action(){}
	~Action();

	void setGoalChange(const std::string& goalName, float change);
	bool requirementsFulfilled(const std::vector<Goal>& goals);
	void addRequirement(const std::string& goalName, float value);
	float getGoalChange(const std::string& goalName);
};

