// "AIUEB3.cpp": Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include "Action.h"
#include "WorldModel.h"
#include "Planer.h"
#include <iostream>


int main()
{
	// "real" goals
	// These are the goals with initial start values that change over time
	Goal zombiesDead("zombies_dead", 5, 0.5f);
	Goal hunger("hunger", 3, 0.5f);
	Goal thirst("thirst", 2, 0.3);
	Goal toilet("toilet", 3.2f, 0.7f);
	Goal sleep("sleep", 3, 0.2f);

	// inbetween goals
	// These are the subgoals with true/false states depicted as float values 1/0 (so i can use the same goal class)
	Goal isCupboardOpen("cupboard_open", 1, 0);
	Goal hasWeapon("has_weapon", 1, 0);
	Goal hasBakedPizza ("has_baked_pizza", 1, 0);
	Goal hasDrink("has_drink", 1, 0);

	// here the actions are defined
	// first parameter is the cost, second the name
	Action killZombie(0.4f, "kill zombie");
	killZombie.setGoalChange(zombiesDead.name, -1);
	killZombie.addRequirement(hasWeapon.name, 0);

	Action getWeapon(0.6f, "get weapon");
	getWeapon.setGoalChange(hasWeapon.name, -1);
	getWeapon.addRequirement(isCupboardOpen.name, 0);

	Action openCupboard(2.0f, "open cupboard");
	openCupboard.setGoalChange(isCupboardOpen.name, -1);

	Action eatPizza(2.5f, "eat pizza");
	eatPizza.setGoalChange(hunger.name, -3);
	eatPizza.setGoalChange(hasBakedPizza.name, 1);
	eatPizza.addRequirement(hasBakedPizza.name, 0);

	Action bakePizza(3.0f, "bake pizza");
	bakePizza.setGoalChange(hasBakedPizza.name, -1);

	Action drinkWater(1, "drink water");
	drinkWater.setGoalChange(thirst.name, -0.5f);
	drinkWater.setGoalChange(hasDrink.name, 1);
	drinkWater.addRequirement(hasDrink.name, 0);

	Action fillGlass(0.8f, "fill glass");
	fillGlass.setGoalChange(hasDrink.name, -1);

	Action goToSleep(12.0f, "go to sleep");
	goToSleep.setGoalChange(sleep.name, -4);

	Action goToToilet(2.4f, "go to toilet");
	goToToilet.setGoalChange(toilet.name, -3);

	// turn goals into vector
	std::vector<Goal> goals =
	{
		zombiesDead, hunger, thirst, toilet, sleep,
		isCupboardOpen, hasWeapon, hasBakedPizza, hasDrink
	};

	// turn actions into vector
	std::vector<Action> actions =
	{
		killZombie, getWeapon, openCupboard, eatPizza,
		bakePizza, drinkWater, fillGlass, goToSleep, goToToilet
	};

	// create goals the planer should fulfill
	// These use the same names as the "real" goals so the planer can check whether a goal value is equivalent to the fulfillment goal value
	Goal allZombiesDead(zombiesDead.name, 2, 0);
	Goal notHungry (hunger.name, 1, 0);
	Goal notThirsty(thirst.name, 1, 0);
	Goal noToilet(toilet.name, 1, 0);
	Goal rested(sleep.name, 1, 0);

	// create a vector out of all the goals that need to be fulfilled in one plan
	std::vector<Goal> goalsToFulfill =
	{
		allZombiesDead, noToilet, rested, notHungry, notThirsty
	};

	// create the worldmodel with the actions and goals
	WorldModel model(actions, goals);
	Planer planer;

	std::cout << "INITIAL WORLDMODEL VALUES" << std::endl;
	model.output();
	std::cout << "-----------------------------------" << std::endl << std::endl;
	
	// let the planer plan an action path for the worldmodel to go through
	std::vector<Action> path = planer.planAction(model, goalsToFulfill);
	if (path.size() <= 0)
	{
		std::cout << "No action found!" << std::endl;
	}
	else
	{
		std::cout << " -----------------------------------" << std::endl;
		std::cout << " GOALS PLANER IS TRYING TO FULFILL" << std::endl;
		for (Goal goal : goalsToFulfill)
		{
			std::cout << goal.name << " to value: " << goal.value << std::endl;
		}
		std::cout << "------------------------------------" << std::endl;
		// output all applied actions and all of the current goal values in the current state
		for (Action action : path)
		{
			model.applyAction(action);
			std::cout << "\nApplied action: " << action.name << std::endl;
			model.output();
		}
	}

    return 0;
}

