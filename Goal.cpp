#include "stdafx.h"
#include "Goal.h"


Goal::Goal(const std::string& name, float value, float timeChange) : name(name), value(value), timeChange(timeChange)
{

}

Goal::Goal(const Goal& obj)
{
	name = obj.name;
	value = obj.value;
	timeChange = obj.timeChange;
}


Goal::~Goal()
{
}
